package com.bicapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BicappApplication {

    public static void main(String[] args) {
        SpringApplication.run(BicappApplication.class, args);
    }

}
