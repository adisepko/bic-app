package com.bicapp.business.domain.workout.plan.template.model;

import com.bicapp.business.domain.workout.exercise.model.Complexity;
import com.bicapp.business.domain.workout.exercise.model.Exercise;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
public class WorkoutPlanTemplate {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String description;
    @Enumerated(EnumType.STRING)
    private Complexity complexity;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(joinColumns = {@JoinColumn(name = "workout_plan_template_id")},
            inverseJoinColumns = {@JoinColumn(name = "exercise_id")})
    private List<Exercise> exercises;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WorkoutPlanTemplate that = (WorkoutPlanTemplate) o;
        return getId().equals(that.getId()) && getName().equals(that.getName())
                && getDescription().equals(that.getDescription()) && getComplexity() == that.getComplexity()
                && Objects.equals(getExercises(), that.getExercises());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getDescription(), getComplexity(), getExercises());
    }
}
