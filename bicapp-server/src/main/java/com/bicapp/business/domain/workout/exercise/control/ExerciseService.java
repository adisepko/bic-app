package com.bicapp.business.domain.workout.exercise.control;

import com.bicapp.business.domain.workout.exercise.model.Complexity;
import com.bicapp.business.domain.workout.exercise.model.ComplexityDto;
import com.bicapp.business.domain.workout.exercise.model.Exercise;
import com.bicapp.business.domain.workout.exercise.model.ExerciseDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Stream;

@Service
public record ExerciseService(ExerciseRepository exerciseRepository) {
    public Exercise createExercise(Exercise exercise) {
        return exerciseRepository.save(exercise);
    }
    public ExerciseDto fidExerciseById(long id) {
        return exerciseRepository.findById(id)
                .map(ExerciseDto::of)
                .orElseThrow(() -> new RuntimeException("Exercise not found"));
    }
    public List<ComplexityDto> getComplexity() {
        return Stream.of(Complexity.values()).map(ComplexityDto::of).toList();
    }
}
