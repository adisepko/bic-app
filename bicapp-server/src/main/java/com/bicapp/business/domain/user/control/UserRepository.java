package com.bicapp.business.domain.user.control;

import com.bicapp.business.domain.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findUserByUsername(String username);
    @Query("select u from User u join fetch u.workoutPlanTemplates where u.id= :id")
    Optional<User> loadWorkoutPlanTemplates(Long id);
}
