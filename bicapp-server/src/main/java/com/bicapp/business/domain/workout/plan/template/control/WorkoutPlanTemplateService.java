package com.bicapp.business.domain.workout.plan.template.control;

import com.bicapp.business.domain.user.control.UserRepository;
import com.bicapp.business.domain.user.control.UsernamePasswordAuthentication;
import com.bicapp.business.domain.user.model.User;
import com.bicapp.business.domain.workout.plan.template.model.WorkoutPlanTemplate;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.ListUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class WorkoutPlanTemplateService {
    private final WorkoutPlanTemplateRepository workoutPlanTemplateRepository;
    private final UserRepository userRepository;

    public WorkoutPlanTemplate createWorkoutPlanTemplate(WorkoutPlanTemplate workoutPlanTemplate) {
        return workoutPlanTemplateRepository.save(workoutPlanTemplate);
    }

    public List<WorkoutPlanTemplate> findAllWorkoutPlanTemplate() {
        return workoutPlanTemplateRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<WorkoutPlanTemplate> findAllWorkoutPlanTemplatesForLoggedInUser(UsernamePasswordAuthentication principal) {
        Long userId = principal.getUser().getId();
        return userRepository.loadWorkoutPlanTemplates(userId)
                .map(User::getWorkoutPlanTemplates)
                .orElse(List.of());
    }

    @Transactional
    public List<WorkoutPlanTemplate> assignWorkoutPlanTemplateToLoggedInUser(UsernamePasswordAuthentication principal,
                                                                             WorkoutPlanTemplate workoutPlanTemplate) {
        User user = principal.getUser();
        Long userId = user.getId();
        List<WorkoutPlanTemplate> workoutPlanTemplates = userRepository.loadWorkoutPlanTemplates(userId)
                .map(User::getWorkoutPlanTemplates)
                .orElse(List.of());
        user.setWorkoutPlanTemplates(ListUtils.union(workoutPlanTemplates, List.of(workoutPlanTemplate)));
        userRepository.save(user);
        return workoutPlanTemplates;
    }
}
