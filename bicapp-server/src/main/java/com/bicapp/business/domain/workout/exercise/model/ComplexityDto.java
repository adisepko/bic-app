package com.bicapp.business.domain.workout.exercise.model;

public record ComplexityDto(String value, String displayName) {
    public static ComplexityDto of(Complexity complexity) {
        return new ComplexityDto(complexity.name(), complexity.getDisplayName());
    }
}
