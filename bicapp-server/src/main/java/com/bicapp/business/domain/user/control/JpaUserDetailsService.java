package com.bicapp.business.domain.user.control;

import com.bicapp.business.domain.user.authorities.control.AuthorityRepository;
import com.bicapp.business.domain.user.authorities.model.Authority;
import com.bicapp.business.domain.user.authorities.model.AuthorityEnum;
import com.bicapp.business.domain.user.model.CustomUserDetails;
import com.bicapp.business.domain.user.model.User;
import com.bicapp.business.domain.user.model.dto.UserDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

@RequiredArgsConstructor
@Slf4j
@Service
public class JpaUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;
    private final BCryptPasswordEncoder encoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Supplier<UsernameNotFoundException> s =
                () -> new UsernameNotFoundException(
                        "Problem during authentication!");

        User u = userRepository
                .findUserByUsername(username)
                .orElseThrow(s);

        return new CustomUserDetails(u);
    }

    @Transactional
    public User createUser(UserDto user) {
        userRepository.findUserByUsername(user.username()).ifPresent((u) -> {
            throw new RuntimeException("User Already Exists!");
        });
        List<AuthorityEnum> authorities = Optional.ofNullable(user.authority())
                .orElse(List.of(AuthorityEnum.TRAINEE));
        List<Authority> userAuthorities = authorities.stream().map(this::mapToAuthority).toList();
        String password = encoder.encode(user.password());
        User u = User.builder()
                .username(user.username())
                .password(password)
                .authorities(userAuthorities)
                .build();
        return userRepository.save(u);
    }

    private Authority mapToAuthority(AuthorityEnum authorityEnum) {
        return authorityRepository.findAuthorityByName(authorityEnum)
                .orElseGet(() -> authorityRepository.save(Authority.of(authorityEnum)));
    }
}

