package com.bicapp.business.domain.user.boundry;

import com.bicapp.business.domain.user.control.UsernamePasswordAuthentication;
import com.bicapp.business.domain.user.model.User;
import com.bicapp.business.domain.user.model.dto.UserInfo;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserInfoController {

    @GetMapping("/user-info")
    public ResponseEntity<UserInfo> getUserInfo(UsernamePasswordAuthentication principal) {
        User user = principal.getUser();
        UserInfo userInfo = UserInfo.of(user);
        return ResponseEntity.ok(userInfo);
    }
}
