package com.bicapp.business.domain.user.authorities.model;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public enum AuthorityEnum {
    TRAINER("Trener"),
    TRAINEE("Osoba Trenująca"),
    TRAINING_FACILITY_ADMIN("Administrator Obiektu Sportowy"),
    ADMIN("Administrator");
    private final String displayName;

    AuthorityEnum(String displayName) {
        this.displayName = displayName;
    }
}
