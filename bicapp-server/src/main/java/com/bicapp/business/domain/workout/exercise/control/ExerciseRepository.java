package com.bicapp.business.domain.workout.exercise.control;

import com.bicapp.business.domain.workout.exercise.model.Exercise;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExerciseRepository extends JpaRepository<Exercise, Long> {
}
