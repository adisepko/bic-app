package com.bicapp.business.domain.workout.exercise.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
public class Exercise {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String description;
    @Enumerated(EnumType.STRING)
    private Complexity complexity;
    @Column
    private Double duration;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Exercise exercise = (Exercise) o;
        return getId().equals(exercise.getId()) && getName().equals(exercise.getName())
                && getDescription().equals(exercise.getDescription()) && getComplexity() == exercise.getComplexity()
                && Objects.equals(getDuration(), exercise.getDuration());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getDescription(), getComplexity(), getDuration());
    }
}
