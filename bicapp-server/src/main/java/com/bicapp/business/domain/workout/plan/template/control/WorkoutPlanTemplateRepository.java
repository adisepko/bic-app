package com.bicapp.business.domain.workout.plan.template.control;

import com.bicapp.business.domain.workout.plan.template.model.WorkoutPlanTemplate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkoutPlanTemplateRepository extends JpaRepository<WorkoutPlanTemplate, Long> {

}
