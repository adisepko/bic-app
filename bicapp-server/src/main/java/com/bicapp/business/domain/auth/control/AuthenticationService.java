package com.bicapp.business.domain.auth.control;

import com.bicapp.business.domain.user.control.JpaUserDetailsService;
import com.bicapp.business.domain.user.control.UsernamePasswordAuthentication;
import com.bicapp.business.domain.user.model.User;
import com.bicapp.business.domain.user.model.dto.UserDto;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import io.vavr.collection.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@Service
public class AuthenticationService {
    private final AuthenticationManager authenticationManager;
    private final JpaUserDetailsService userService;
    @Value("${jwt.signing.key}")
    private String signingKey;

    public AuthenticationService(AuthenticationManager authenticationManager,
                                 JpaUserDetailsService userService) {
        this.authenticationManager = authenticationManager;
        this.userService = userService;
    }

    public String authenticate(UserDto user) {
        List<Object> objects = new List<>();
        objects.forEachWithIndex();
        String username = user.username();
        String password = user.password();
        UsernamePasswordAuthentication usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthentication(username, password);
        authenticationManager.authenticate(usernamePasswordAuthenticationToken);
        return buildJwt(username);
    }

    public String createUser(UserDto userDto) {
        User user = userService.createUser(userDto);
        String username = user.getUsername();
        return buildJwt(username);
    }

    private String buildJwt(String username) {
        SecretKey key = Keys.hmacShaKeyFor(signingKey.getBytes(StandardCharsets.UTF_8));
        return Jwts.builder()
                .setClaims(Map.of("username", username))
                .signWith(key)
                .compact();
    }

    public ResponseEntity<?> authorizedResponse(HttpServletResponse response, String jwt) {
        Cookie cookie = createAuthCookie(jwt, 7 * 24 * 60 * 60);
        response.addCookie(cookie);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    public ResponseEntity<?> expireAuthCookie(HttpServletResponse response, String jwt) {
        Cookie cookie = createAuthCookie(jwt, 0);
        response.addCookie(cookie);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    private Cookie createAuthCookie(String jwt, int maxAge) {
        Cookie cookie = new Cookie("auth-token", jwt);
        cookie.setMaxAge(maxAge);
        cookie.setHttpOnly(true);
        cookie.setPath("/");
        return cookie;
    }
}
