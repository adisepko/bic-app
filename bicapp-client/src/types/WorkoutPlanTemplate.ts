export default interface WorkoutPlanTemplate {
  id: number;
  name: string;
  description: string;
  complexity: string;
  exercises: [];
}
