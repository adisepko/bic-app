export default interface User {
  id: null;
  username: string;
  password: string;
}
