export default interface Exercise {
  id: null;
  name: string;
  description: string;
  complexity: string;
  duration: number;
}
