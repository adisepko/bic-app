import http from "@/system/http-commons";
import User from "@/types/User";

class AuthenticationService {
  register(user: User) {
    return http.post("/register", user);
  }

  login(user: User) {
    return http.post("/login", user);
  }

  signOut() {
    return http.post("/sign-out", null);
  }
}

export default new AuthenticationService();
