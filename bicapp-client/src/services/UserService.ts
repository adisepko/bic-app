import http from "@/system/http-commons";

class UserService {
  getUserInfo() {
    return http.get("/user-info");
  }
}
export default new UserService();
