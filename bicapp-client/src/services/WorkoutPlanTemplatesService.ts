import http from "@/system/http-commons";

class WorkoutPlanTemplatesService {
  fetchAllWorkoutPlanTemplates() {
    return http.get("/workout-plan-template");
  }
}

export default new WorkoutPlanTemplatesService();
