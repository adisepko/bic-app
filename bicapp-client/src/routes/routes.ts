import { createWebHistory, createRouter } from "vue-router";
import { RouteRecordRaw } from "vue-router";
const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "home",
    component: () => import("../components/HomePage.vue"),
  },
  {
    path: "/register",
    name: "register",
    component: () => import("../components/SignUp.vue"),
  },
  {
    path: "/login",
    name: "login",
    component: () => import("../components/Login.vue"),
  },
  {
    path: "/workout-plan-template",
    name: "workout-plan-template",
    component: () => import("../components/WorkoutPlanTemplates.vue"),
  },
  {
    path: "/exercise",
    name: "exercise-form",
    component: () => import("../components/ExerciseForm.vue"),
  },
  {
    path: "/exercise/:id",
    name: "exercise-detail",
    component: () => import("../components/Exercise.vue"),
  },
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});
export default router;
